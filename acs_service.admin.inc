<?php

/**
 * Implements hook_form().
 */
function acs_service_admin($form, &$form_state) {
  
  $form['username'] = array(
    '#type'=>'textfield', 
    '#title' =>t('Username'),
    '#default_value' => variable_get('ACS_SERVICE_USERNAME', 'username'),

     );

  $form['password'] = array(
    '#type'=>'textfield', 
    '#title' =>t('Password'),
    '#default_value' => variable_get('ACS_SERVICE_PASSWORD', 'password'),
     );

  $form['access_production_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Production Access Token'),
    '#default_value' => variable_get('ACS_PRODUCTION_KEY', 'Production Key'),
  );

  $form['access_dev_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Development Access Token'),
    '#default_value' => variable_get('ACS_DEVELOPMENT_KEY', 'Development Key'),
  );

  $form['server_type'] = array(
    '#type' => 'select',
    '#options' => array(
    	'0'=>t('Production'),
    	'1'=>t('Development')
    	),
    '#title' => t('Access Token'),
    '#default_value' => variable_get('ACS_SERVICE_TYPE', 1),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save') );

  return $form;
}

/**
 * Implements hook_form().
 */

function acs_send_notify($form, &$form_state){
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Message Title'),
    //'#default_value' => variable_get('ACS_PRODUCTION_KEY', 'Production Key'),
  );

  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message Body'),
    //'#default_value' => variable_get('ACS_DEVELOPMENT_KEY', 'Development Key'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
    );

  return $form;
}

function acs_service_admin_submit($form,&$form_state){


  variable_set('ACS_PRODUCTION_KEY', $form_state['values']['access_production_token']); // According to Titanium Studio
  variable_set('ACS_DEVELOPMENT_KEY', $form_state['values']['access_dev_token']); // According to Titanium Studio
  variable_set('ACS_SERVICE_USERNAME', $form_state['values']['username']);
  variable_set('ACS_SERVICE_PASSWORD', $form_state['values']['password']);
  variable_set('ACS_SERVICE_TYPE', $form_state['values']['server_type']);
  
  drupal_set_message(t('All data Saved ..'), 'status', FALSE);
}


function acs_send_notify_submit($form,&$form_state){

      /*** SETUP ***************************************************/
     $server_type = variable_get('ACS_SERVICE_TYPE');

     switch ($server_type) {
       case '1':
         # Development Case
       $key        = variable_get('ACS_DEVELOPMENT_KEY','');//"YOUR_APP_KEY";         //GET FROM APP MANAGEMENT (ACS)
         break;
       case '0':
         # Production Case
       $key        = variable_get('ACS_PRODUCTION_KEY','');//"YOUR_APP_KEY";         //GET FROM APP MANAGEMENT (ACS)
         break;
       default:
         # code...
         break;
     }

    
    $username   = variable_get('ACS_SERVICE_USERNAME', '');   //"YOUR_LOGIN";
    $password   = variable_get('ACS_SERVICE_PASSWORD', ''); //"YOUR_PASSWORD";
    $channel    = "PUSH_CHANNEL";
    $message    = $form_state['values']['message'];//"YOUR_MESSAGE";
    $title      = $form_state['values']['title'];//"YOUR_ANDROID_TITLE";
    $tmp_fname  = 'cookie.txt';
    $json       = '{"alert":"'. $message .'","title":"'. $title .'","vibrate":true,"sound":"default"}';
 
    /*** PUSH NOTIFICATION ***********************************/
 
    $post_array = array('login' => $username, 'password' => $password);
 
    /*** INIT CURL *******************************************/
    $curlObj    = curl_init();
    $c_opt      = array(CURLOPT_URL => 'https://api.cloud.appcelerator.com/v1/users/login.json?key='.$key,
                        CURLOPT_COOKIEJAR => $tmp_fname, 
                        CURLOPT_COOKIEFILE => $tmp_fname, 
                        CURLOPT_RETURNTRANSFER => true, 
                        CURLOPT_POST => 1,
                        CURLOPT_POSTFIELDS  =>  "login=".$username."&password=".$password,
                        CURLOPT_FOLLOWLOCATION  =>  1,
                        CURLOPT_TIMEOUT => 60);
 
    /*** LOGIN **********************************************/
    curl_setopt_array($curlObj, $c_opt); 
    $session = curl_exec($curlObj);     
 
    /*** SEND PUSH ******************************************/
    $c_opt[CURLOPT_URL]         = "https://api.cloud.appcelerator.com/v1/push_notification/notify.json?key=".$key; 
    $c_opt[CURLOPT_POSTFIELDS]  = "channel=".$channel."&payload=".$json; 
 
    curl_setopt_array($curlObj, $c_opt); 
    $session = curl_exec($curlObj);     
 
    /*** THE END ********************************************/
    curl_close($curlObj);


    echo $session;
      drupal_set_message(t('All messages sent ..'), 'status', FALSE);
}